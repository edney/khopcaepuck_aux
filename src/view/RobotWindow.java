package view;

import georegression.struct.shapes.Quadrilateral_F64;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;

import controller.Epuck;
import controller.Fiducial;
import controller.Topology;

public class RobotWindow extends JFrame {
	Topology topology;
	public static boolean runningAlgorithm;
	public static boolean updatedTopology;
	public int radiationRadius;
	public List<Quadrilateral_F64> target;
	CamWindow camWindow;
	public static boolean updated;
	public static ArrayList<Integer>weight;
		
	public RobotWindow(CamWindow camWindow, int fiducialQuantity){
		this.camWindow = camWindow;
		this.radiationRadius = camWindow.getRadiationRadius();
		target = new ArrayList<Quadrilateral_F64>();
		topology = new Topology();
		initUI();
		runningAlgorithm = false;
		updatedTopology = false;
		weight = new ArrayList<Integer>(Collections.nCopies(fiducialQuantity, 0));
		updated = false;
		
		
	}

	private void initUI() {
		 
		JLabel comPortEpuckLabel = new JLabel("Porta:");
		final JTextField comPortEpuck = new JTextField(5);
		JButton addEpuck = new JButton("Add Epuck");
		
		JLabel rangeProxLabel = new JLabel("Range Proximity:");
		final JTextField rangeProx = new JTextField(5);
		JButton sendRange = new JButton("Send Range");
				
		DefaultListModel model = new DefaultListModel();
		final JList listComPort = new JList(model);
			    
		JButton start = new JButton("Start");
	    JButton stop = new JButton("Stop");
	    JButton getID = new JButton("get ID");
	    
	    JLabel algorithmLabel = new JLabel("Set Algorithm:");
	    final JButton khopca = new JButton("Start KHOPCA");
	    
	    setLayout(new FlowLayout());
	    add(comPortEpuckLabel);
	    add(comPortEpuck);
	    add(addEpuck);
	    add(start);
	    add(stop);
	    add(getID);
	    add(algorithmLabel);
	    add(khopca);
	    add(rangeProxLabel);
	    add(rangeProx);
	    add(sendRange);
	    add(listComPort);
	    
	    topology.addEpuck(new Epuck("/dev/rfcomm0"));
	    topology.addEpuck(new Epuck("/dev/rfcomm1"));
	    topology.addEpuck(new Epuck("/dev/rfcomm2"));
	    
	    topology.addEpuck(new Epuck("/dev/rfcomm3"));
	    topology.addEpuck(new Epuck("/dev/rfcomm4"));
	    topology.addEpuck(new Epuck("/dev/rfcomm5"));
	    
	    
	    topology.addEpuck(new Epuck("/dev/rfcomm6"));
	    topology.addEpuck(new Epuck("/dev/rfcomm7"));
	    topology.addEpuck(new Epuck("/dev/rfcomm8"));
	    	    
	    model.add(0, "/dev/rfcomm0_2948");
	    model.add(1, "/dev/rfcomm1_3167");
	    model.add(2, "/dev/rfcomm2_3080");
	    
	    model.add(3, "/dev/rfcomm3_3040");
	    model.add(4, "/dev/rfcomm4_3118");
	    model.add(5, "/dev/rfcomm5_3177");
	    
	    model.add(6, "/dev/rfcomm6_3054");
	    model.add(7, "/dev/rfcomm7_2816");
	    model.add(8, "/dev/rfcomm8_2926");
	    
	    Map<Integer, String> map = new HashMap<Integer, String>();
	    
	    map.put(0, "2948");
	    map.put(1, "3167");
	    map.put(2, "3080");
	    
	    map.put(3, "3040");
	    map.put(4, "3118");
	    map.put(5, "3177");
	    
	    map.put(6, "3054");
	    map.put(7, "2816");
	    map.put(8, "2926");
	    
	    topology.setMapFiducial(map);
	    	    
	    addEpuck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                topology.addEpuck(new Epuck(comPortEpuck.getText()));
            	
                if (!comPortEpuck.getText().isEmpty() && !comPortEpuck.getText().isEmpty()) {
                	
                	DefaultListModel model = (DefaultListModel) listComPort.getModel();
                	model.add(model.getSize(), comPortEpuck.getText());
                	comPortEpuck.setText("");
				}
            }
        });
		
	    start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	
            	for(int i= 0; i< topology.getEpuckList().size(); i++){
            		try {
						topology.getEpuckList().get(i).sendStart();
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
            	}
            }
        });
	    
	    stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	for(int i= 0; i< topology.getEpuckList().size(); i++){
            		try {
						topology.getEpuckList().get(i).sendStop();
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
            	}
            }
        });
	    	    
	    getID.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	
            	new Thread() {
					@Override
					public void run() {
						
						for(int i= 0; i< topology.getEpuckList().size(); i++){
		            		try {
								topology.getEpuckList().get(i).getId();
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
		            	}
					}
            	}.start();
            }
        });
	    
	    sendRange.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	for(int i= 0; i< topology.getEpuckList().size(); i++){
            		try {
						topology.getEpuckList().get(i).sendProximityRange(rangeProx.getText());
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
            	}
            }
        });
	    
	    khopca.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	
            	if(!runningAlgorithm){
            		runningAlgorithm = true;
					khopca.setText("Stop  KHOPCA");
					
					new Thread() {
						@Override
						public void run() {
					                        		
							try {
								
								for(int i= 0; i< topology.getEpuckList().size(); i++){
				            		try {
										topology.getEpuckList().get(i).getId();
										Thread.sleep(100);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
				            	}
								runKhopca();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}	
					}.start();
            	}else{
            		runningAlgorithm = false;
            		khopca.setText("Start KHOPCA");
            	}
            }
        });
	    
	
	
	   
	    
	    
	}
	
	public void process() throws InterruptedException{
		
		for (int i = 0; i < topology.getEpuckList().size(); i++) {
			topology.getEpuckList().get(i).setFiducial(new Fiducial(0,0, 0));
		}
		
		
		while (true) {
			
			//topology.removeFiducials();
			
			for (int i = 0; i < camWindow.getFiducialList().size(); i++) {
				
				if(camWindow.getFiducialList().get(i).isActive()){
					topology.setFiducial(camWindow.getFiducialList().get(i));
					
					//System.out.println("id do fiducial: " + topology.getEpuckList().get(i).getFiducial().getId() + " id do epuck: " + topology.getEpuckList().get(i).getId() 
					//		+ " active: " +topology.getEpuckList().get(i).getFiducial().isActive());
				}
				//else{
					//topology.removeFiducial(i);
				//}
				
				
				
			}	
		    	
			 
			 
			 
			 
			 
			/* if(camWindow.getFiducialList() != null){
		    		for (int i = 0; i < camWindow.getFiducialList().size(); i++) {
						if(camWindow.getFiducialList().get(i).isActive()){
							topology.setFiducial(camWindow.getFiducialList().get(i));
							
						}
						
						
					}
		    		
		    		for (int i = 0; i < topology.getEpuckList().size(); i++) {
		    			if(topology.getEpuckList().get(i).getFiducial() != null){
		    				System.out.println("id do fiducial: " + topology.getEpuckList().get(i).getFiducial().getId() + " id do epuck: " + topology.getEpuckList().get(i).getId() 
		    						+ topology.getEpuckList().get(i).getFiducial().isActive());
		    			}
						
					}
		    		
		    	}*/
		    	
		}
	}
	
	
	public Topology getTopology() {
		return topology;
	}

	public void setTopology(Topology topology) {
		this.topology = topology;
	}
			
	public int getRadiationRadius() {
		return radiationRadius;
	}

	public void setRadiationRadius(int radiationRadius) {
		this.radiationRadius = radiationRadius;
	}
	
	public static boolean runningAlgorithm(){
		return runningAlgorithm;
	}
	
	public static boolean updatedTopology(){
		return updatedTopology;
	}
	
	public List<Quadrilateral_F64> getTarget() {
		return target;
	}

	public void setTarget(List<Quadrilateral_F64> target) {
		this.target = target;
	}

	protected void runKhopca() throws InterruptedException { 
		
		//////List<String> listK = new ArrayList<>();
				
		while(runningAlgorithm){
			
			topology.update(camWindow.getFiducialList(),radiationRadius);
			
			updated = true;
			
			//for (int i = 0; i < camWindow.getFiducialList().size(); i++) {
			//	if (camWindow.getFiducialList().get(i).isActive()) {
			//		System.out.print(i +" esta ativo - ");
			//		camWindow.getFiducialList().get(i).setKhapa(8);
			//	}
			//	System.out.println();
				
			//}
			
			//for(int i= 0; i< topology.getEpuckList().size(); i++){
        		
        	//////		topology.update(radiationRadius);       			
         	//////		CamWindow.listK.set(i, topology.getEpuckById("00" + CamWindow.listDataMatrix.get(i)).getK());
         			
        			//listK.add(topology.getEpuckById("00" + CamWindow.listDataMatrix.get(i)).getK());
         			//Thread.sleep(300);
					
				
        	///////}
			//CamWindow.listK = new ArrayList<String>(listK);
			//System.out.println(listK.get(0) + "-" + listK.get(1) + "-" + listK.get(2));
			///////updatedTopology = true;
			//listK.clear();
		}
	}
}