package controller;

import java.util.ArrayList;
import java.util.Random;

public class Epuck {

	private String portName;
	private String id;
	private ArrayList<Epuck> neighbors;
	private String k;
	private double positionX;
	private double positionY;
	private BluetoothHandler bluetooth;
	private Message message;
	private Fiducial fiducial;

	public Epuck(String portName) {
		super();
		this.portName = portName;
		neighbors = new ArrayList<Epuck>();
		bluetooth = new BluetoothHandler(portName);
		bluetooth.connect();
		this.message = new Message();
		Random rand = new Random();
		this.k = String.valueOf(rand.nextInt(9) + 1);
	}

	public static void main(String[] args) throws InterruptedException {
		//Epuck epuck1 = new Epuck("COM7");
		//try {
			//epuck1.sendStart();
			//epuck1.getId();
			//epuck1.sendProximityRange("250");
			//epuck1.sendStop();
			//String teste = convertValueToFormatedString(78);
			//System.out.println(teste);
			
		//} catch (InterruptedException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
		//epuck1.sendProximityRange("020");
			
		//Epuck epuck1 = new Epuck("/dev/tty.e-puck_3040-COM1");
		//Epuck epuck2 = new Epuck("/dev/tty.e-puck_2948-COM1");
		Epuck epuck0 = new Epuck("/dev/rfcomm0");
		Thread.sleep(1500);
		Epuck epuck1 = new Epuck("/dev/rfcomm1");
		Thread.sleep(1500);
		Epuck epuck2 = new Epuck("/dev/rfcomm2");
		Thread.sleep(1500);
		Epuck epuck3 = new Epuck("/dev/rfcomm3");
		Thread.sleep(1500);
		Epuck epuck4 = new Epuck("/dev/rfcomm4");
		Thread.sleep(1500);
		Epuck epuck5 = new Epuck("/dev/rfcomm5");
		Thread.sleep(1500);
		Epuck epuck6 = new Epuck("/dev/rfcomm6");
		Thread.sleep(1500);
		Epuck epuck7 = new Epuck("/dev/rfcomm7");
		Thread.sleep(1500);
		Epuck epuck8 = new Epuck("/dev/rfcomm8");
		Thread.sleep(1500);
		ArrayList<Epuck> lista = new ArrayList<>();
		lista.add(epuck0);
		lista.add(epuck1);
		lista.add(epuck2);
		lista.add(epuck3);
		lista.add(epuck4);
		lista.add(epuck5);
		lista.add(epuck6);
		lista.add(epuck7);
		lista.add(epuck8);
		
		while(true){
			for (int i = 0; i < lista.size(); i++) {
				
				while(!lista.get(i).getBluetooth().isFlag()){
					lista.get(i).getBluetooth().connect();
					System.out.println("tentou");
				}
				String k = lista.get(i).getK();
				Thread.sleep(50);
				System.out.println("Valor de K-: " + i +"- " + k);
				
				
				
			}
		}
		
		
		
		//epuck1.setId("003040");
		//epuck2.setId("002948");
		//epuck3.setId("003167");
		
		//ArrayList<Epuck> lista = new ArrayList<>();
		//epuck2.setK("003");
		//epuck3.setK("003");
		//lista.add(epuck2);
		//lista.add(epuck3);
		
		//epuck1.setNeighbors(lista);
		
		//while(true){
		//	String result = epuck3.getK();
		//	System.out.println(result);
		//}
		
	}
	
	public void sendProximityRange(final String range) {
		new Thread() {
			@Override
			public void run() {
		
				//bluetooth.connect();
				byte[] msg = message.SEND_PROX;
				
				byte[] byteRange = range.getBytes();
				msg[5] = byteRange[0];
				msg[6] = byteRange[1];
				msg[7] = byteRange[2];
						
				bluetooth.sendBytes(msg);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
			
					e.printStackTrace();
				}
				
				//bluetooth.disconnect();
				System.out.println("mandou o range:" + msg.toString());
			}
		}.start();
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getId() throws InterruptedException {
		
		if(this.id != null){
			return id;
		}else{
		
		//bluetooth.connect();
		byte[] msg = message.REQUEST_ID;
		bluetooth.sendBytes(msg);
		
		Thread.sleep(50);
		
		byte[] idReceived = bluetooth.readBytes(6);
		String str = new String(idReceived);
		id = str;
		//bluetooth.disconnect();
		System.out.println("pegou o id:" + id);
		}
			
		return id;
	}
	
	public void sendStart() throws InterruptedException {
		
		
				//bluetooth.connect();
				byte[] msg = message.SEND_START;
				bluetooth.sendBytes(msg);
				//bluetooth.disconnect();
				System.out.println("enviou o START");
			
	}
	
	public void sendStop() throws InterruptedException {
		
				//bluetooth.connect();
				byte[] msg = message.SEND_STOP;
				bluetooth.sendBytes(msg);
				//bluetooth.disconnect();
				System.out.println("enviou o STOP");
			
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Epuck> getNeighbors() {
		return neighbors;
	}

	public void setNeighbors(ArrayList<Epuck> neighbors) {
		this.neighbors = neighbors;
	}
	
	public void addNeighbor(Epuck neighbor){
		this.neighbors.add(neighbor);
	}
	
	public void removeNeighbor(Epuck neighbor){
		
		for (int i = 0; i < neighbors.size(); i++) {
			if(neighbors.get(i).getPortName().equals(neighbor.getPortName())){
				this.neighbors.remove(i);
			}
		}
	}

	public String getK() throws InterruptedException {
				
				//if (fazer um codicional para rodar o khopca no computador ou no robot) {
				//	int k = khopca(this.k, this.getNeighbors());
				//	return k;
				//}else{
				//	roda esse outro de baixo
				//}	
		
		
		
		long t1 = System.nanoTime();
		
		
				//bluetooth.connect();
				
				byte[] msg1 = message.REQUEST_K;
				
				String quantityNeighbors = convertValueToFormatedString(getNeighbors().size());
				byte[] qNeighbors = quantityNeighbors.getBytes();
				msg1[5] = qNeighbors[0];
				msg1[6] = qNeighbors[1];
				msg1[7] = qNeighbors[2];
						
				
				
				
				bluetooth.sendBytes(msg1);
				
				
				for (int i = 0; i < getNeighbors().size(); i++) {
					
					//Thread.sleep(50);
					
					byte[] msg2 = message.SEND_NEIGHBORS;
					
					String NeighborIDValue = convertValueToFormatedStringID(Integer.parseInt(getNeighbors().get(i).id));
					byte[] NeighborID = NeighborIDValue.getBytes();
					msg2[4] = NeighborID[0];
					msg2[5] = NeighborID[1];
					msg2[6] = NeighborID[2];
					msg2[7] = NeighborID[3];
									
					bluetooth.sendBytes(msg2);
					
					//Thread.sleep(50);
					
					byte[] msg3 = message.SEND_NEIGHBORS;
					
					String NeighborKValue = convertValueToFormatedString(Integer.parseInt(getNeighbors().get(i).k));
					byte[] NeighborK = NeighborKValue.getBytes();
					msg3[5] = NeighborK[0];
					msg3[6] = NeighborK[1];
					msg3[7] = NeighborK[2];
									
					bluetooth.sendBytes(msg3);
				}
				
				
				long t2 = System.nanoTime();
				long tfinal = t2 - t1;
				//System.out.println("tempo total: " + tfinal/1000000);
				//System.out.println("quantidade neighbors:" + getNeighbors().size());
				
				
				//Thread.sleep(50);
				
				byte[] kReceived = bluetooth.readBytes(6);
				String str = new String(kReceived).substring(3, 6);
				k = str;
				
				//bluetooth.disconnect();
				
				if(this.fiducial != null){
					this.fiducial.setKhapa(Integer.parseInt(k));
				}
					
				
				
				//System.out.println("terminou processo get K");
		return k;
	}
	
	
	public String getK(String param) {
		
		String parts[] = param.split("&");
		
		byte[] msg1 = message.REQUEST_K;
		
		byte[] qNeighbors = parts[0].getBytes();
		msg1[5] = qNeighbors[0];
		msg1[6] = qNeighbors[1];
		msg1[7] = qNeighbors[2];
		bluetooth.sendBytes(msg1);
		
		if(Integer.parseInt(parts[0]) != 0){
			String[] parameters = parts[1].split(",");
			
			for (int i = 0; i < Integer.parseInt(parts[0])*2; i++) {
				
				byte[] msg2 = message.SEND_NEIGHBORS;
				
				String NeighborIDValue = parameters[i];
				byte[] NeighborID = NeighborIDValue.getBytes();
				msg2[4] = NeighborID[0];
				msg2[5] = NeighborID[1];
				msg2[6] = NeighborID[2];
				msg2[7] = NeighborID[3];
								
				bluetooth.sendBytes(msg2);
				
				i++;
				
				byte[] msg3 = message.SEND_NEIGHBORS;
				
				String NeighborKValue = parameters[i];
				byte[] NeighborK = NeighborKValue.getBytes();
				msg3[5] = NeighborK[0];
				msg3[6] = NeighborK[1];
				msg3[7] = NeighborK[2];
								
				bluetooth.sendBytes(msg3);
			}
		}
		
		byte[] kReceived = bluetooth.readBytes(6);
		String str = new String(kReceived).substring(3, 6);
		k = str;
		
		//bluetooth.disconnect();
		
		if(this.fiducial != null){
			this.fiducial.setKhapa(Integer.parseInt(k));
		}
		//System.out.println("terminou processo get K");
return k;
	}
	
	

	public void setK(String k) {
		this.k = k;
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(double positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(double positionY) {
		this.positionY = positionY;
	}
	
	public void setPositionXY(double x, double y){
		this.positionX = x;
		this.positionY = y;
	}

	public void addNeighbors(Epuck epuck) {
		this.neighbors.add(epuck);
	}

	public void removeNeighbors(Epuck epuck) throws InterruptedException {
		for (int i = 0; i < this.neighbors.size(); i++) {
			if (this.neighbors.get(i).getId().equals(epuck.getId())) {
				this.neighbors.remove(i);
			}
		}
	}
	
	public static String convertValueToFormatedString(int value){
		
		StringBuilder resultValue = new StringBuilder("000");
		String converted = String.valueOf(value);
		
		for(int i = 1; i <= converted.length(); i++){
			resultValue.setCharAt(resultValue.length()-i, converted.charAt(converted.length()-i));
		}
		return resultValue.toString();
	}
	
	public static String convertValueToFormatedStringID(int value){
		
		StringBuilder resultValue = new StringBuilder("0000");
		String converted = String.valueOf(value);
		
		for(int i = 1; i <= converted.length(); i++){
			resultValue.setCharAt(resultValue.length()-i, converted.charAt(converted.length()-i));
		}
		return resultValue.toString();
	}
	
	public Fiducial getFiducial() {
		return fiducial;
	}

	public void setFiducial(Fiducial fiducial) {
		this.fiducial = fiducial;
	}

	public BluetoothHandler getBluetooth() {
		return bluetooth;
	}

	
	
	
	
}