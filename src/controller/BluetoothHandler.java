package controller;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

public class BluetoothHandler{

	SerialPort serialPort;
	private static int BAUDRATE = 115200;
	private boolean flag;

	public BluetoothHandler(String portName) {
		super();
		serialPort = new SerialPort(portName);
	}

	public void connect() {

		try {

			if (serialPort != null && serialPort.isOpened()) {
				serialPort.purgePort(1);
				serialPort.purgePort(2);
				serialPort.closePort();
			}

			flag = serialPort.openPort();// Open port
			while (!serialPort.isOpened()){
				flag = serialPort.openPort();// Open port
			}
			
			serialPort.setParams(BAUDRATE, SerialPort.DATABITS_8,SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);// Set params
			
			
		} catch (SerialPortException ex) {
			System.out.println(ex);
			flag = false;
		}
	}

	public boolean disconnect() {
		
		boolean result = true;
		try {
			while (serialPort != null && serialPort.isOpened()) {
				serialPort.purgePort(1);
				serialPort.purgePort(2);
				result = serialPort.closePort();
			}

		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public void sendBytes(byte[] msg) {

		try {
			serialPort.writeBytes(msg);
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public byte[] readBytes(int numberOfBytes) {
		byte[] realBytes = new byte[numberOfBytes];
		try {
			realBytes = this.serialPort.readBytes(numberOfBytes,10000);
		} catch (SerialPortException | SerialPortTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return realBytes;
	}
		
	public SerialPort getSerialPort() {
		return serialPort;
	}

	public void setSerialPort(SerialPort serialPort) {
		this.serialPort = serialPort;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	
	
	
}