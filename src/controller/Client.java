package controller;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.Scanner;

class Client {
	
	Socket skt;
	BufferedReader in;
	PrintWriter out;
		
	public Client(String ip, int port) throws UnknownHostException, IOException {
		super();
		this.skt = new Socket(ip, port);
		out = new PrintWriter(skt.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
	}

	public String request(String msg) throws IOException, InterruptedException{
		
		out.println(msg);
		Thread.sleep(10);
		while (!in.ready()) {}
		String response =in.readLine(); 
		return response;
	}
	



public static void main(String args[]) {
      try {
         @SuppressWarnings("resource")
		//Socket skt = new Socket("192.168.1.37", 1234);
        Socket skt = new Socket("192.168.1.30", 1234);
         PrintWriter out = new PrintWriter(skt.getOutputStream(), true);
         BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
         
         while (true) {
        	//Scanner scanner = new Scanner( System.in );  
 			//int value = scanner.nextInt(); // para inteiros 
 			
 			//if (value != 0) {
 				out.println("send to server \n");
 			//}
 			
 			Thread.sleep(100);	
 				
 			while (!in.ready()) {}
 			System.out.println("Has a response to receive");
 	        System.out.println(in.readLine()); // Read one line and output it
 		}
          //System.out.print("Received string: '");

         //while (!in.ready()) {}
         //System.out.println(in.readLine()); // Read one line and output it

         //System.out.print("'\n");
         //in.close();
      }
      catch(Exception e) {
         System.out.print("Whoops! It didn't work!\n");
      }
   }
}
